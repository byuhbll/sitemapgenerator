package edu.byu.hbll.sitemapgenerator.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application class.
 */
@ApplicationPath("")
public class SitemapGeneratorApplication extends Application {

}
