package edu.byu.hbll.sitemapgenerator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.FilenameUtils;

import edu.byu.hbll.sitemapgenerator.exception.ImproperFileException;

/**
 * Utility class that handles compressing/uncompressing files using the gzip format
 * 
 * @author aluke306
 */
public class GzipUtil {
	
	/**
	 * Checks whether a file is compressed (just checks the extension)
	 * 
	 * @param file file to check
	 * @return true if the files ends in a .gz extension, false otherwise
	 */
	private boolean isCompressed(File file) {
		return FilenameUtils.getExtension(file.getPath()).equals("gz");
	}

	/**
	 * Compresses a file into the gzip format. Appends a ".gz" to the end of the input file path and saves the compressed file there.
	 * 
	 * @param input file to compress
	 * @throws IOException 
	 * @throws FileNotFoundException if file doesn't exist
	 */
	public void compressFile(File input) throws FileNotFoundException, IOException {
		byte[] buffer = new byte[1024];
		String output = input + ".gz";

		try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(output));
				FileInputStream in = new FileInputStream(input)) {

			int len;
			while ((len = in.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
		}
	}

	/**
	 * Extracts a file from the gzip format. Removes the ".gz" from the end of the input file path and saves the extracted file there.
	 * 
	 * @param input file to 
	 * @throws FileNotFoundException file to extract
	 * @throws IOException
	 * @throws ImproperFileException if input is not a .gz file
	 */
	public void extractFile(File input) throws FileNotFoundException, IOException, ImproperFileException {
		if(!isCompressed(input)) {
			throw new ImproperFileException("File " + input + " does not have the extention .gz and cannot be extracted.");
		}

		byte[] buffer = new byte[1024];
		String output = FilenameUtils.removeExtension(input.getPath());
		
		try(GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(input));
				FileOutputStream out = new FileOutputStream(output)){
		
			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
		}
	}
	
	/**
	 * Compresses all non-compressed files in a given directory.
	 * 
	 * @param dir directory to compress files in
	 * @param deleteOld whether or not to remove the old files
	 * @throws ImproperFileException if passed a file that is not a directory
	 * @throws IOException
	 */
	public void compressAllFiles(File dir, boolean deleteOld) throws ImproperFileException, IOException {
		if(!dir.isDirectory()) {
			throw new ImproperFileException("File " + dir + " is not a directory.");
		}
		
		File[] files = dir.listFiles();
		for(File file : files){
			if(file.isFile() && !isCompressed(file)) {
				compressFile(file);
				
				if(deleteOld) {
					file.delete();
				}
			}
		}
	}
	
	/**
	 * Extracts all compressed files in a given directory.
	 * 
	 * @param dir directory to compress files in
	 * @param deleteOld whether or not to remove the old files
	 * @throws ImproperFileException if passed a file that is not a directory
	 * @throws IOException
	 */
	public void extractAllFiles(File dir, boolean deleteOld) throws ImproperFileException, IOException {
		if(!dir.isDirectory()) {
			throw new ImproperFileException("File " + dir + " is not a directory.");
		}
		
		File[] files = dir.listFiles();
		for(File file : files){
			if(file.isFile() && isCompressed(file)) {
				extractFile(file);
				
				if(deleteOld) {
					file.delete();
				}
			}
		}
	}
	
}
