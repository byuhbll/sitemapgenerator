package edu.byu.hbll.sitemapgenerator;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.FileUtils;
import org.jdom2.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

import edu.byu.hbll.sitemapgenerator.exception.ImproperFileException;
import edu.byu.hbll.sitemapgenerator.exception.SitemapException;
import edu.byu.hbll.sitemapgenerator.model.HarvestedRecords;
import edu.byu.hbll.sitemapgenerator.model.Record;
import edu.byu.hbll.sitemapgenerator.util.GzipUtil;
import edu.byu.hbll.sitemapgenerator.util.JsonUtil;
import edu.byu.hbll.sitemapgenerator.util.SitemapXMLUtil;


public class SitemapGenerator {

	// big classes
	static private Logger logger =  LoggerFactory.getLogger(SitemapGenerator.class);
	private Client client;
	
	// fields to be read from config file
	private Map<String, List<String>> allowedRecords;
	private int generateTo;
	private String sitemapURL;
	private File sitemapDir;
	private int maxTries;
	private boolean deleteNoncompressedSitemaps;
	
	// utils and other helpful classes
	private SitemapXMLUtil xmlUtil;
	private GzipUtil gzipUtil;
	
	// other class fields
	private File tempDir;
	private int sitemapNum = 0;
	private String fillerUrl;
	
	// definitions and variables not likely to change (and doesn't necessarily need to be placed in config yaml file)
	private static final String RECORD_LOCATION = "https://search.lib.byu.edu/{institution}/record/{recId}";
	private static final int URLS_PER_SITEMAP = 50000;
	private static final String WEB_SERVICE_URI = "https://probe.lib.byu.edu:8443/green/{institution}/harvest/ids?limit=100000"; // TODO change number :p

	/**
	 * Default constructor, used mostly in testing
	 */
	public SitemapGenerator() {
		client = ClientBuilder.newClient();
		
		this.allowedRecords = new HashMap<String, List<String>>();
		this.xmlUtil = new SitemapXMLUtil();
		this.gzipUtil = new GzipUtil();
	}
	
	/**
	 * Takes in values we'll need to run the generation algorithm.
	 * 
	 * @param allowedRecords a map of an institution to which types of records we should index
	 * @param generateTo the total number of sitemaps we should generate
	 * @param sitemapURL used by the index to reference the location of the sitemap files
	 * @param sitemapDir location where sitemap files should be saved after generation completes
	 * @param maxTries maximum number of times we should try to harvest from a URI
	 * @param deleteNoncompressedSitemaps whether we should delete non-compressed version of a sitemap
	 */
	public SitemapGenerator(Map<String, List<String>> allowedRecords, int generateTo, String sitemapURL, File sitemapDir,
			int maxTries, boolean deleteNoncompressedSitemaps) {
		
		this();
		
		this.allowedRecords = allowedRecords;
		this.generateTo = generateTo;
		this.sitemapURL = sitemapURL;
		this.sitemapDir = sitemapDir;
		this.maxTries = maxTries;
		this.deleteNoncompressedSitemaps = deleteNoncompressedSitemaps;
	}
	
	/**
	 * Loops through each institution, and generates compressed sitemap files for each. Also creates an index file for the sitemaps.
	 * The main method of SitemapGenerator. 
	 * 
	 * @throws InterruptedException if the thread is interrupted while sleeping
	 * @throws IOException
	 * @throws SitemapException if URLS_PER_SITEMAP is greater than the maximum allowed urls per sitemap
	 * @throws ImproperFileException if we attempt to improperly compress a file
	 */
	public void generate() throws InterruptedException, IOException, SitemapException, ImproperFileException {
		logger.info("------------------------------------------------------");
		logger.info("STARTING SITEMAP GENERATION");
		logger.debug("Calling generate()...");
		
		tempDir = Files.createTempDir();
		
		// loop through each institution, harvest and read records from each
		for(String institution : allowedRecords.keySet()) {
			readRecords(institution);
		}
		logger.info("Generated " + sitemapNum + " significant sitemaps.");
		
		// generate filler urls, continuing from whatever number we finished making "real" sitemaps from
		List<String> fillerUrls = new LinkedList<>();
		fillerUrls.add(fillerUrl);
		for(int i = sitemapNum + 1; i <= generateTo; i++) {
			String sitemapPath = tempDir.getPath() + File.separator + "sitemap" + i + ".xml";
			writeSitemap(fillerUrls, sitemapPath);
		}
		logger.info("Total number of sitemaps generated is " + generateTo + ".");
		
		// compress all of the generated sitemaps
		gzipUtil.compressAllFiles(tempDir, deleteNoncompressedSitemaps);
		
		// generate and write sitemap index
		List<String> sitemaps = new LinkedList<>();
		for(int i = 1; i <= generateTo; i++) {
			String sitemap = sitemapURL + File.separator + "sitemap" + i + ".xml.gz";
			sitemaps.add(sitemap);
		}
		writeIndex(sitemaps, tempDir.getPath() + File.separator + "sitemap.xml");
		
		// create final directory, then copy contents from temp and delete temp
		File finalDir = createDirectory(sitemapDir);
		FileUtils.copyDirectory(tempDir, finalDir);
		FileUtils.deleteDirectory(tempDir);
		
		logger.info("SUCCESSFULLY ENDING SITEMAP GENERATION");
	}
	
	/**
	 * Harvests and writes records from a given institution. Will wait 10 minutes if we encounter an error while trying to proccess
	 * the data.
	 * 
	 * @param institution the institution to harvest records from (i.e. "byu")
	 * @throws InterruptedException if the thread is interrupted while sleeping
	 * @throws IOException 
	 * @throws SitemapException if URLS_PER_SITEMAP is greater than the maximum allowed urls per sitemap
	 */
	public void readRecords(String institution) throws InterruptedException, IOException, SitemapException {
		logger.debug("Calling readRecords()...");
		logger.debug("\tParam institution=" + institution);
		
		// init variables
		List<String> urls = new LinkedList<>();
		URI uri = UriBuilder.fromUri(WEB_SERVICE_URI).build(institution);
		boolean done = false;
		int tries = 0;
		
		// keep harvesting and writing as long as api tells us we
		while(!done) {
			try {
				String json = client.target(uri).request().get(String.class);
				HarvestedRecords harvested = JsonUtil.fromJson(json, HarvestedRecords.class);
				
				// loop through the records we harvested
				for(Record record : harvested.getRecords()) {
					// check that the record is one we want to put into a sitemap
					if(isAllowedRecord(record, allowedRecords.get(institution))) {
						urls.add(buildRecordURL(institution, record));
						fillerUrl = buildRecordURL(institution, record);
					}
					
					// if the sitemap capacity is reached, write urls to file, then reset
					if(urls.size() >= URLS_PER_SITEMAP) {
						sitemapNum++;
						String sitemapPath = tempDir.getPath() + File.separator + "sitemap" + sitemapNum + ".xml";
						writeSitemap(urls, sitemapPath);
						urls.clear();
					}
				}
				
				// update uri, whether we're done reading records, and reset number of tries
				uri = new URI(harvested.getNextUri());
				done = harvested.isDone();
				tries = 0;
				
			} catch(Exception e) {
				logger.info("Request to " + uri + " has failed! Will attempt retry " + (tries + 1) + " of " + maxTries + ".");
				logger.error(e.toString(), e);
				requestFailed(tries, maxTries);
				tries++;
			}
		}
		
		// if we have any records remaining in our list, write to file
		if(!urls.isEmpty()) {
			sitemapNum++;
			String sitemapPath = tempDir.getPath() + File.separator + "sitemap" + sitemapNum + ".xml";
			writeSitemap(urls, sitemapPath);
			urls.clear();
		}
	}
	
	/**
	 * Logic for when a request fails. If we hit the max number of tries, throw an exception; otherwise, just wait for 10 minutes.
	 * 
	 * @param tries number of times we've tried to make this request
	 * @param MAX_TRIES the maximum number of tries we want to attempt.
	 * @throws InterruptedException if the sleeping thread is interrupted
	 */
	public void requestFailed(int tries, final int MAX_TRIES) throws InterruptedException {
		logger.debug("Calling requestFailed()...");
		logger.debug("\tParam tries=" + tries);
		logger.debug("\tParam MAX_TRIES=" + MAX_TRIES);
		
		if(tries >= MAX_TRIES) {
			InternalServerErrorException e = new InternalServerErrorException("Could not connect to server after " + tries + " tries.");
			logger.error(e.toString(), e);
			throw e;
			
		} else {
			TimeUnit.MINUTES.sleep(10); // sleep for 10 minutes before trying again
		}
	}
	
	/**
	 * Checks whether a record should be put in a sitemap file or not.
	 * 
	 * @param record record to check
	 * @param allowedPrefixes a list of approved prefixes (a record must have this prefix to be put in a sitemap file)
	 * @return true if the record starts with one of the allowed prefixes, false otherwise
	 */
	public boolean isAllowedRecord(Record record, List<String> allowedPrefixes) {
		for(String prefix : allowedPrefixes) {
			if(record.getRecordId().startsWith(prefix + ".")) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Builds the url for a record.
	 * 
	 * @param institution the institution this record belongs to
	 * @param record the record the build the url for
	 * @return a properly formatted URL for the record
	 */
	public String buildRecordURL(String institution, Record record) {
		return UriBuilder.fromUri(RECORD_LOCATION).build(institution, record.getRecordId()).toString();
	}
	
	/**
	 * Builds a sitemap out of a list of urls and compresses the file.
	 * 
	 * @param urls a list of urls to be put into the sitemap
	 * @param sitemapPath path where the file should be saved, should not have .gz extention
	 * @throws SitemapException if the list of urls is too large
	 * @throws IOException
	 */
	public void writeSitemap(List<String> urls, String sitemapPath) throws SitemapException, IOException {
		logger.debug("Calling writeSitemap()...");
		logger.debug("\tParam urls.size=" + urls.size());
		logger.debug("\tParam sitemapPath=" + sitemapPath);
		
		Document doc = xmlUtil.buildSitemapDocument(urls);
		xmlUtil.writeDocument(doc, sitemapPath);
		//gzipUtil.compressFile(new File(sitemapPath)); // TODO zip here?
	}
	
	/**
	 * Builds the sitemap index.
	 * 
	 * @param sitemaps a list of sitemaps to be put into the index
	 * @param indexPath path where the file should be saved
	 * @throws SitemapException if the list of sitemaps is too large
	 * @throws IOException
	 */
	public void writeIndex(List<String> sitemaps, String indexPath) throws SitemapException, IOException {
		logger.debug("Calling writeIndex()...");
		logger.debug("\tParam indexPath=" + indexPath);
		
		Document doc = xmlUtil.buildIndexDocument(sitemaps);
		xmlUtil.writeDocument(doc, indexPath);
	}
	
	/**
	 * Creates a directory at path dirName. In the case directory already exists, cleans out contents of directory.
	 * 
	 * @param dir location of directory to be created/cleaned
	 * @return File pointer to created directory
	 * @throws IOException if cleaning of file unsuccessful (from FileUtils.cleanDirectory())
	 */
	public File createDirectory(File dir) throws IOException {
		logger.debug("Calling createDirectory()...");
		logger.debug("\tParam dir=" + dir);
		
		if(dir.exists() && dir.isDirectory()) {
			FileUtils.cleanDirectory(dir);
		} else {
			dir.mkdir();
		}
		return dir;
	}

}
