package edu.byu.hbll.sitemapgenerator.exception;

public class ImproperFileException extends Exception {

	private static final long serialVersionUID = 1L;

	public ImproperFileException() {
		super();
	}
	
	public ImproperFileException(String message) {
		super(message);
	}

}
