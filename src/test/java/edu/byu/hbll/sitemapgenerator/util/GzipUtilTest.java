package edu.byu.hbll.sitemapgenerator.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import edu.byu.hbll.sitemapgenerator.exception.ImproperFileException;

public class GzipUtilTest {
	
	private GzipUtil gzip = new GzipUtil();
	private File tempDir;
	
	private final static String separator = File.separator;
	
	@Before
	public void init() {
		tempDir = Files.createTempDir();
	}
	
	@After
	public void tearDown() throws IOException {
		FileUtils.deleteDirectory(tempDir);
	}
	
	@Test
	public void compressFileShouldThrowExceptionIfFileDoesNotExist() throws IOException {		
		try {
			File input = new File(tempDir + separator + "input.txt");
			gzip.compressFile(input);
			
		} catch(FileNotFoundException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void compressFileShouldCreateCompressedFile() throws FileNotFoundException, IOException {
		File input = File.createTempFile("input", ".txt", tempDir);
		File compressed = new File(input + ".gz");
		
		gzip.compressFile(input);
		assertTrue(compressed.exists());
	}
	
	@Test
	public void extractFileShouldThrowExceptionIfFileIsNotCompressed() throws FileNotFoundException, IOException {
		try {
			File input = File.createTempFile("input", ".txt", tempDir);
			gzip.extractFile(input);
			
		} catch(ImproperFileException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void extractFileShouldThrowExceptionIfFileDoesNotExist() throws IOException, ImproperFileException {
		try {
			File input = new File(tempDir + separator + "input.txt.gz");
			gzip.extractFile(input);
			
		} catch(FileNotFoundException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void extractFileShouldCreateExtractedFile() throws IOException, ImproperFileException {
		// the test needs a file that is actually compressed, so we compress input
		File input = File.createTempFile("input", ".txt", tempDir);
		gzip.compressFile(input);
		File compressed = new File(input + ".gz");

		// if we extract the compressed file, we expect to get back our input file
		gzip.extractFile(compressed);
		assertTrue(input.exists());
	}
	
	@Test
	public void compressAllFilesShouldThrowExceptionIfPassedANonDirectory() throws IOException {
		try {
			File input = new File(tempDir + separator + "input.txt.gz");
			gzip.compressAllFiles(input, false);
			
		} catch(ImproperFileException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void compressAllFilesShouldCompressAllFilesInDirectoryAndKeepOldIfPassedFalse() throws IOException, ImproperFileException {
		File a = File.createTempFile("aInput", ".txt", tempDir);
		File b = File.createTempFile("bInput", ".txt", tempDir);
		File aCompressed = new File(a + ".gz");
		File bCompressed = new File(b + ".gz");
		
		gzip.compressAllFiles(tempDir, false);
		
		assertTrue(a.exists());
		assertTrue(b.exists());
		assertTrue(aCompressed.exists());
		assertTrue(bCompressed.exists());
	}
	
	@Test
	public void compressAllFilesShouldCompressAllFilesInDirectoryAndDeleteOldIfPassedTrue() throws IOException, ImproperFileException {
		File a = File.createTempFile("aInput", ".txt", tempDir);
		File b = File.createTempFile("bInput", ".txt", tempDir);
		File aCompressed = new File(a + ".gz");
		File bCompressed = new File(b + ".gz");
		
		gzip.compressAllFiles(tempDir, true);
		
		assertFalse(a.exists());
		assertFalse(b.exists());
		assertTrue(aCompressed.exists());
		assertTrue(bCompressed.exists());
	}
	
	@Test
	public void extractAllFilesShouldThrowExceptionIfPassedFileNotDirectory() throws IOException {
		try {
			File notDir = new File(tempDir.getPath() + separator + "temp.txt");
			gzip.extractAllFiles(notDir, false);
			
		} catch(ImproperFileException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void extractAllFilesShouldExtractAllFilesInDirectoryAndKeepOldIfPassedFalse() throws IOException, ImproperFileException {
		// compress files so we can extract them later
		File a = File.createTempFile("aInput", ".txt", tempDir);
		File b = File.createTempFile("bInput", ".txt", tempDir);
		gzip.compressAllFiles(tempDir, true); // pass true here so we just have compressed files
		
		// extract files and check what's in the dir
		gzip.extractAllFiles(tempDir, false);
		assertEquals(4, tempDir.list().length);
		assertTrue(a.exists());
		assertTrue(b.exists());
	}
	
	@Test
	public void extractAllFilesShouldExtractAllFilesInDirectoryAndDeleteOldIfPassedTrue() throws IOException, ImproperFileException {
		// compress files so we can extract them later
		File a = File.createTempFile("aInput", ".txt", tempDir);
		File b = File.createTempFile("bInput", ".txt", tempDir);
		gzip.compressAllFiles(tempDir, true); // pass true here so we just have compressed files
		
		// extract files and check what's in the dir
		gzip.extractAllFiles(tempDir, true);
		assertEquals(2, tempDir.list().length);
		assertTrue(a.exists());
		assertTrue(b.exists());
	}

}
