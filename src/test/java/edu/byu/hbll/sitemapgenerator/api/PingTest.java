package edu.byu.hbll.sitemapgenerator.api;

import static org.junit.Assert.*;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;

public class PingTest {

	@Test
	public void testPing() {
		try {
			ZonedDateTime.parse(new Ping().ping());
		}
		catch(DateTimeParseException e) {
			fail("Ping did not return a valid timestamp.");
		}
	}

}
